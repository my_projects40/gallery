#pragma once
#include "IDataAccess.h"
#include "sqlite3.h"
#include <iostream>


class DatabaseAccess : public IDataAccess
{
public:
	DatabaseAccess() = default;
	virtual ~DatabaseAccess() = default;
	//part 1
	bool open() override;
	void close() override;
	void clear() override;
	void closeAlbum(Album& pAlbum) override;
	//part 2
	void deleteAlbum(const std::string& albumName, int userId) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void createUser(User& user) override;
	void deleteUser(const User & user) override;
	//part 3
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	bool doesAlbumExists(const std::string& albumName,int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void printAlbums() override;
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void printUsers() override;
	User getUser(int userId) override;
	bool doesUserExists(int userId) override;
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;
	
	const std::list<Picture> getPictures(int album_id);

private:
	sqlite3* _db;
}; 

static int callback(void* data, int argc, char** argv, char** azColName);
static int callback_users(void* data, int argc, char** argv, char** azColName);
static int callback_picture(void* data, int argc, char** argv, char** azColName);
static int countAlbumsOwnedOfUser_callback(void* data, int argc, char** argv, char** azColName);
static int callback_tags(void* data, int argc, char** argv, char** azColName);