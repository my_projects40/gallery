#include "DatabaseAccess.h"
#include "sqlite3.h"
#include <iostream>
#include <io.h>
#include <map>
#include <algorithm>

///****************Part 1**********************///

/*
* This function opens the database if it exist and if it not so just creats a new database
* input: none
* output: true/false
*/
bool DatabaseAccess::open()
{
	
	char* errMsg = nullptr;
	std::string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &_db);
	if (res != SQLITE_OK)
	{
		_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	//if exist
	if (doesFileExist != 0) {
		
		char* errMsg = nullptr;
		int tbl_users = sqlite3_exec(_db, "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL)", nullptr, nullptr, &errMsg);
		if (tbl_users != SQLITE_OK)
			return false;
		else
			std::cout << "Records created Successfully!" << std::endl;


		int tbl_albums = sqlite3_exec(_db, "CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, CREATION_DATE DATE NOT NULL, USER_ID INTEGER NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID))", nullptr, nullptr, &errMsg);
		if (tbl_albums != SQLITE_OK)
			return false;
		else
			std::cout << "Records created Successfully!" << std::endl;
			

		int tbl_pictures = sqlite3_exec(_db, "CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE DATE NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID))", nullptr, nullptr, &errMsg);
		if (tbl_pictures != SQLITE_OK)
			return false;
		else
			std::cout << "Records created Successfully!" << std::endl;
		int tbl_tags = sqlite3_exec(_db, "CREATE TABLE TAGS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,  PICTURE_ID INTEGER NOT NULL, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID), USER_ID INTEGER NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID))", nullptr, nullptr, &errMsg);
		if (tbl_tags != SQLITE_OK)
			return false;
		else
			std::cout << "Records created Successfully!" << std::endl;
	}

	return true;
}
/*
* This function closes the connection with db
* input: none
* output: none
*/
void DatabaseAccess::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}
void DatabaseAccess::clear()
{
}
void DatabaseAccess::closeAlbum(Album& album)
{
}

///****************Part 2**********************///

/*
* This function delets the Album of user
* input: const std::string& albumName, int userId
* output: none
*/
void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	char* errMsg = nullptr;
	std::string al = "DELETE FROM ALBUMS WHERE USER_ID = '" + std::to_string(userId) + "'AND NAME = '" + albumName + "';";
	int rem = sqlite3_exec(_db,  al.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot delete the album" << std::endl;
		sqlite3_free(errMsg);
	}
}



/*
* This function adds tags to a picture
* input: const std::string& albumName, const std::string& pictureName, int userId
* output: none
*/
void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMsg = nullptr;
	std::string tag = "INSERT INTO TAGS ('USER_ID', 'PICTURE_ID')VALUES('" + std::to_string(userId) + "', 'SELECT ID FROM PICTURES WHERE NAME = " + pictureName + "AND ALBUM_ID IN(SELECT ID FROM ALBUM WHERE NAME = )" + albumName + "AND USER_ID = " + std::to_string(userId) + "');";
	int rem = sqlite3_exec(_db, tag.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot create the user" << std::endl;
		sqlite3_free(errMsg);
	}
}
/*
* This function removes tags form pictures
* input: const std::string& albumName, const std::string& pictureName, int userId
* output: none
*/
void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMsg = nullptr;
	std::string tag = "DELETE FROM TAGS WHERE USER_IDVALUES = '" + std::to_string(userId) + "'AND PICTURE_ID = SELECT ID FROM PICTURES WHERE NAME = '" + pictureName + "'AND ALBUM_ID IN(SELECT ID FROM ALBUM WHERE NAME = )'" + albumName + "'AND USER_ID = '" + std::to_string(userId) + "');";
	int rem = sqlite3_exec(_db, tag.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot create the user" << std::endl;
		sqlite3_free(errMsg);
	}
}


/*
* This function creates the User in database
* input: User& user
* output: none
*/
void DatabaseAccess::createUser(User& user)
{
	char* errMsg = nullptr;
	std::string us = "INSERT INTO USERS(ID, NAME)VALUES('" + std::to_string(user.getId()) + "','" + user.getName() +"');";
	int rem = sqlite3_exec(_db, us.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot create the user" << std::endl;
		sqlite3_free(errMsg);
	}
}

/*
* This function delets user from database
* input: const User& user
* output: none
*/
void DatabaseAccess::deleteUser(const User& user)
{
	char* errMsg = nullptr;
	std::string del = "DELETE FROM USERS WHERE ID = '" + std::to_string(user.getId()) + "'AND NAME = '" + user.getName() + "';";
	int rem = sqlite3_exec(_db, del.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot delete the user" << std::endl;
		sqlite3_free(errMsg);
	}
}     




///****************Part 3**********************///



static int callback_picture(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* pictures = (std::list<Picture>*) data;
	Picture picture;
	std::string name;
	int id = 0;
	for (int i = 0; i < argc; i++)
	{
		if ((std::string(azColName[i]) == "NAME"))
			name = argv[i];
		else if ((std::string(azColName[i]) == "ID"))
			id = std::stoi(argv[i]);
		
	}
	picture = Picture(id, name);
	pictures->push_back(picture);
	return SQLITE_OK;
}
const std::list<Picture> DatabaseAccess::getPictures(int album_id)
{
	std::list<Picture> pictures;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM PICTURES WHERE ALBUM_ID = '" + std::to_string(album_id) +"';";
	int rem = sqlite3_exec(_db, crt.c_str(), callback_picture, &pictures, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot select all pictures" << std::endl;
		sqlite3_free(errMsg);
	}
	return pictures;
}




static int callback(void* data, int argc, char** argv, char** azColName)
{
	std::list<Album>* albums = (std::list<Album>*) data;
	Album album;
	for (int i = 0; i < argc; i++)
	{
		if ((std::string(azColName[i]) == "NAME"))
			album.setName(argv[i]);
		else if ((std::string(azColName[i]) == "CREATION_DATE"))
			album.setCreationDate(argv[i]);
		else if ((std::string(azColName[i]) == "USER_ID"))
			album.setOwner(atoi(argv[i]));

	}

	albums->push_back(album);
	return SQLITE_OK;
}
/*
* This function returns the albums
* input: none
* output: albums
*/

const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> albums;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM ALBUMS;";
	int rem = sqlite3_exec(_db, crt.c_str(), callback, &albums, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot select all albums" << std::endl;
		sqlite3_free(errMsg);
	}
	for (auto i = albums.begin(); i != albums.end(); ++i)
	{
		i->setPictures(this->getPictures(i->getOwnerId()));
	}
	return albums;
}

/*
* This function returns the Albums
* input: const User& user
* output: albums
*/
const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albums;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM ALBUMS WHERE USER_ID = '" + std::to_string(user.getId()) + "';";
	int rem = sqlite3_exec(_db, crt.c_str(), &callback, &albums, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot get Albums Of User" << std::endl;
		sqlite3_free(errMsg);
	}
	for (auto i = albums.begin(); i != albums.end(); ++i)
	{
		i->setPictures(this->getPictures(i->getOwnerId()));
	}
	return albums;
}

/*
* This function creats Album in database
* input: Album& album
* output: none
*/
void DatabaseAccess::createAlbum(const Album& album)
{
	char* errMsg = nullptr;
	std::string crt = "INSERT INTO ALBUMS(NAME, CREATION_DATE, USER_ID)VALUES('" + album.getName() + "','" + album.getCreationDate() + "','" + std::to_string(album.getOwnerId()) + "');";
	int rem = sqlite3_exec(_db, crt.c_str(), nullptr, nullptr, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot create the album" << std::endl;
		std::cerr << errMsg << std::endl;
		sqlite3_free(errMsg);
	}
}

/*
* This function checks if user's album exist
* input: const std::string& albumName, int userId
* output: true/false
*/
bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	std::list<Album> albums = getAlbums();
	for (auto const& album : albums)
		if (album.getName() == albumName && album.getOwnerId() == userId)
			return true;

	return false;
}


static int callback_tags(void* data, int argc, char** argv, char** azColName)
{
	std::list<int>* tags = (std::list<int>*) data;

	for (int i = 0; i < argc; i++)
	{
		if ((std::string(azColName[i]) == "USER_ID"))
			tags->push_back(atoi(argv[i]));
	}

	return SQLITE_OK;
}

/*
* This function opens(returns) the album by a givven name
* input: const std::string& albumName
* output: *(--i) - album
*/
Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	Album album;
	std::list<Album> albums;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM ALBUMS WHERE NAME = '" + albumName  + "';";
	int rem = sqlite3_exec(_db, crt.c_str(), &callback, &albums, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << errMsg << std::endl;
		std::cerr << "Cannot open Album" << std::endl;
		sqlite3_free(errMsg);
	}
	album = albums.front();

	std::list<Picture> pictures;

	for (auto& pic : pictures)
	{
		std::list<int> tags;
		crt = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = '" + std::to_string(pic.getId()) + "';";
		rem = sqlite3_exec(_db, crt.c_str(), callback_tags, &tags, &errMsg);
		if (rem != SQLITE_OK)
		{
			std::cerr << "error in database";
			sqlite3_free(errMsg);
		}
		for (auto& tag : tags)
		{
			pic.tagUser(tag);
		}
	
		album.setPictures(this->getPictures(album.getOwnerId()));
	}
	return album;
}

/*
* This function prints the Albums
* input: none
* output: none
*/
void DatabaseAccess::printAlbums()
{
	std::list<Album> albums;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM ALBUMS;";
	int rem = sqlite3_exec(_db, crt.c_str(), &callback, &albums, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot print Albums" << std::endl;
		sqlite3_free(errMsg);
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;

	for (const Album& album : albums)
	{
		std::cout << std::setw(5) << "* " << album;
	}

}


/*
* This function adds Picture To Album By Name
* input: const std::string& albumName, const Picture& picture
* output: none
*/
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	char* errMessage = nullptr;
	time_t now = time(nullptr);


	std::string sqlStatement = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES ('" + picture.getName() + "', '" + picture.getPath() + "', '" + std::ctime(&now) + "', (SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "'));";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cerr << errMessage;
		std::cerr << "Could not delete add Picture To Album By Name\n";
		sqlite3_free(errMessage);
	}

}
/*
* This function removes Picture From Album By Name
* input: const std::string& albumName, const std::string& pictureName
* output: none
*/
void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string sqlStatement = "DELETE FROM PICTURES WHERE NAME = '" + pictureName + "' AND ALBUM_ID = SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		std::cerr << "Could not remove Picture From Album By Name\n";
		sqlite3_free(errMessage);
	}

}



static int callback_users(void* data, int argc, char** argv, char** azColName)
{
	std::list<User>* users = (std::list<User>*) data;
	User user = User(atoi(argv[0]), argv[1]);
	for (int i = 0; i < argc; i++)
	{
		if ((std::string(azColName[i]) == "NAME"))
			user.setName(argv[i]);
		
	}
	users->push_back(user);
	return SQLITE_OK;
}

/*
* This function prints the users
* input: none
* output: none
*/
void DatabaseAccess::printUsers()
{
	std::list<User> users;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM USERS;";
	int rem = sqlite3_exec(_db, crt.c_str(), &callback, &users, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot print Users" << std::endl;
		sqlite3_free(errMsg);
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;

	for (const User& user : users)
	{
		std::cout << std::setw(5) << "* " << user;
	}
}

/*
* This function return the user by his id
* input: userId
* output: *(--i) - user
*/
User DatabaseAccess::getUser(int userId)
{
	std::list<User> users;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM USERS WHERE ID = '" + std::to_string(userId) + "';";
	int rem = sqlite3_exec(_db, crt.c_str(), &callback_users, &users, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot get User" << std::endl;
		sqlite3_free(errMsg);
	}
	if (users.empty())
	{
		User user;
		return user;
	}
	return users.front();
}

/*
* This function checks if user exist
* input: int userId
* output: true/false
*/
bool DatabaseAccess::doesUserExists(int userId)
{
	User user = getUser(userId);
	if (user.getId() != -1)//if user no exist his id is -1
		return true;

	return false;
}


static int countAlbumsOwnedOfUser_callback(void* data, int argc, char** argv, char** azColName)
{
	int* count = (int*)data;
	if (argc < 1)
	{
		return -1;
	}
	*count = atoi(argv[0]);
	
	return SQLITE_OK;
}
/*
* This function counts the amout of usres' albums
* input: const User& user
* output: count
*/
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int count = 0;
	
	char* errMsg = nullptr;
	
	std::string crt = "SELECT COUNT(USER_ID) FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	int rem = sqlite3_exec(_db, crt.c_str(), &countAlbumsOwnedOfUser_callback, &count, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot count Albums Owned Of User" << std::endl;
		sqlite3_free(errMsg);
	}
	
	return count;
}





/*
* This function count Albums Tagged Of User
* input: const User& user
* output: count
*/
int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	std::list<Album> albums;
	int albumsCount = 0;

	for (const auto& album : albums) {
		const std::list<Picture>& pics = album.getPictures();

		for (const auto& picture : pics) {
			if (picture.isUserTagged(user)) {
				albumsCount++;
				break;
			}
		}
	}

	return albumsCount;

}
/*
* This function returns the count Tags Of User
* input: const User& user
* output: count
*/
int DatabaseAccess::countTagsOfUser(const User& user)
{
	int count = 0;
	std::list<User> users;
	char* errMsg = nullptr;
	std::string crt = "SELECT * FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	int rem = sqlite3_exec(_db, crt.c_str(), &callback, &users, &errMsg);
	if (rem != SQLITE_OK)
	{
		std::cerr << "Cannot count Tags Of User" << std::endl;
		sqlite3_free(errMsg);
	}
	for (const User& user : users)
	{
		count++;
	}

	return count;

}
/*
* This function returns the average Tags Per Album Of User
* input: const User& user
* output: static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount
*/
float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if (0 == albumsTaggedCount) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}
User DatabaseAccess::getTopTaggedUser()
{
	std::list<Album> albums;
	std::map<int, int> userTagsCountMap;

	auto albumsIter = albums.begin();
	for (const auto& album : albums) {
		for (const auto& picture : album.getPictures()) {

			const std::set<int>& userTags = picture.getUserTags();
			for (const auto& user : userTags) {
				//As map creates default constructed values, 
				//users which we haven't yet encountered will start from 0
				userTagsCountMap[user]++;
			}
		}
	}

	if (userTagsCountMap.size() == 0) {
		std::cout << "There isn't any tagged user.";
	}

	int topTaggedUser = -1;
	int currentMax = -1;
	for (auto entry : userTagsCountMap) {
		if (entry.second < currentMax) {
			continue;
		}

		topTaggedUser = entry.first;
		currentMax = entry.second;
	}

	if (-1 == topTaggedUser) {
		std::cout << "Failed to find most tagged user";
	}

	return getUser(topTaggedUser);
}
Picture DatabaseAccess::getTopTaggedPicture()
{
	std::list<Album> albums;
	int currentMax = -1;
	const Picture* mostTaggedPic = nullptr;
	for (const auto& album : albums) {
		for (const Picture& picture : album.getPictures()) {
			int tagsCount = picture.getTagsCount();
			if (tagsCount == 0) {
				continue;
			}

			if (tagsCount <= currentMax) {
				continue;
			}

			mostTaggedPic = &picture;
			currentMax = tagsCount;
		}
	}
	if (nullptr == mostTaggedPic) {
		std::cout << "There isn't any tagged picture.";
	}

	return *mostTaggedPic;
}
std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Album> albums;
	std::list<Picture> pictures;

	for (const auto& album : albums) {
		for (const auto& picture : album.getPictures()) {
			if (picture.isUserTagged(user)) {
				pictures.push_back(picture);
			}
		}
	}

	return pictures;
}